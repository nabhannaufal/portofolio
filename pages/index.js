import React from "react";
import Head from "next/head";

export default function Home() {
  return (
    <>
      <Head>
        <title>Nabhan Naufal</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <div className="container">
        <div className="card">
          <div className="name">Nabhan Naufal</div>
          <hr />
          <div className="title">Web Developer</div>
          <div className="email">nabhannaufal@gmail.com</div>
          <div className="phone">0818666040</div>
          <div className="card-back"></div>
          <div className="card-front"></div>
          <div className="card2">
            <div className="card-pink"></div>
          </div>
        </div>
      </div>
      <div className="circle1"></div>
      <div className="circle2"></div>
      <div className="circle3"></div>
      <div className="circle4"></div>
      <div className="circle5"></div>
      <div className="circle6"></div>
      <div className="circle7"></div>
      <div className="circle8"> </div>
    </>
  );
}
